package org.ancestra.evolutive.common;

public interface Action {
	public void applyAction();
	public boolean actionNeeded();
}